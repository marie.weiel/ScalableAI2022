import sys
import time
import h5py
import numpy as np
from mpi4py import MPI
np.random.seed(842424) # Fix random seed for reproducibility.


def sigmoid(z):
    return 1.0 / (1.0+np.exp(-z))

def lr_predict(W, X):
    """
    Return prediction of logit model for training data X using model weights W.

    Parameters
    ----------
    X : array-like, shape = [n_samples, n_features +1]
        Training dataset (after bias trick).
    W : array-like, shape = [n_features + 1, ]
        Parameters (after bias trick).

    W: weights to be learned. There is one weight for every input dimension plus a bias.
    X: input data where the 0th input should be 1.0 to take the bias into account in a simple dot product.

    Returns:
    ----------
    array-like, shape = [n_samples, ]
    Predicted activations Y_est of logit model for training dataset X,
    in this case the sigmoid of dot product of weights and input data.
    """
    return sigmoid(X @ W)

def mse(Yest, Y):
    """"
    Compute mean-square-error loss.
    
    Parameters
    ----------
    Yest : array-like, shape = [n_samples, ]
           Predictions.
    Y : array-like, shape = [n_samples, ]
        Ground-truth labels.
        
    Returns
    ----------
    loss: MSE loss
    """
    N = Y.shape[0]                      # Get number of samples in considered batch. 
    loss = (1./N) * (Y-Yest).T @ (Y-Yest) # Calculate MSE loss for considered batch.
    return loss

def lr_loss(W, X, Y):
    """Return the loss and the gradient with respect to the weights.
    
    Parameters
    ----------
    W : weights of the model to be learned, where weights[0] is the bias
    X : input data of shape [N x D+1], 0th element of each sample is assumed to be 1
    Y : ground-truth labels of shape [N,]
    
    Returns
    -----------
    loss : scalar mean-square-error loss for batch of samples
    gradient : np.array([weights.shape]) 
               gradient of loss with respect to weights
    """
    N = Y.shape[0]          # Get number of samples in batch.
    Yest = lr_predict(W, X) # Compute logit prediction for all samples in batch. 
    
    loss = mse(Yest, Y)     # Compute MSE loss over all samples in batch.
    # Compute gradient vector of loss w.r.t. weights.
    gradient = (-2./N) * ((Y - Yest) * Yest * Yest * np.exp(-X @ W)).T @ X
    return loss, gradient

def lr_train(W, X, Y, epochs=100, eta=0.001, b=10):
    """
    Train the model, i.e. update the weights following the negative gradient until the model converges.
    
    Parameters
    ----------
    W : weights of the model to be learned, where weights[0] is the bias
    X : input data of shape [N x D+1], where each sample's 0th element is assumed to be 1 for bias trick
    Y : ground-truth labels of shape [N,]
    epochs : number of epochs to be trained
    eta : learning rate
    b : batch size
    
    Returns
    -----------
    weights : trained weights
    """

    comm = MPI.COMM_WORLD
    size = comm.size
    rank = comm.rank

    # Apply bias trick.
    N = Y.shape[0] # Determine number of samples in batch.
    Nb = N // b    # Determine number of full batches in data (drop last).
    print("Rank", rank, "/", size, ": Data is divided into", Nb, "batches.")
    loss_history = np.zeros(epochs)
    acc_history = np.zeros(epochs)
    te = 0.0 # Inititate training time per epoch.

    for epoch in range(epochs): # Loop over epochs.
    # The number of epochs is a hyperparameter of gradient descent 
    # that controls the number of complete passes through the training dataset.
    # The batch size is a hyperparameter of gradient descent 
    # that controls the number of training samples to work through before the 
    # model’s internal parameters are updated.
    
        loss_sum = 0.0 # Initiate loss for each epoch.
        accuracy = 0.0 # Initiate accuracy for each epoch.
       
        start = time.perf_counter()

        for nb in range(Nb):
            x = X[nb*b:(nb+1)*b]
            y = Y[nb*b:(nb+1)*b]
            loss, gradient = lr_loss(W, x, y)
            loss_sum += loss
            
            corr = np.sum((lr_predict(W, x) + 0.5).astype(int) == y)
            accuracy += corr
            Gradient = np.zeros_like(gradient)
            comm.Allreduce(gradient, Gradient, op=MPI.SUM)
            Gradient /= size
            W -= eta * Gradient

        end = time.perf_counter()

        # Calculate loss + accuracy after each epoch.
        loss_sum /= Nb
        accuracy /= N
        accuracy *= 100
        
        Loss_sum = comm.allreduce(loss_sum, op=MPI.SUM)
        Loss_sum /= size
        Accuracy = comm.allreduce(accuracy, op=MPI.SUM)
        Accuracy /= size
        
        loss_history[epoch] = Loss_sum
        acc_history[epoch] = Accuracy
        te += (end -  start)


        # Print every tenth epoch the training status.
        if rank == 0:
            if epoch % 10 == 0:
                print('Epoch: {:2}, Loss: {:.5f}, Accuracy: {}'.format(epoch, Loss_sum, Accuracy))
    te /= epochs
    Te = comm.allreduce(te, op=MPI.SUM)
    Te /= size
    return W, loss_history, acc_history, Te

# MAIN STARTS HERE.

comm = MPI.COMM_WORLD
size = comm.size
rank = comm.rank

epochs = int(sys.argv[1]) # Get number of epochs from command line.
beff = int(sys.argv[2])   # Get effective batch size from command line.

if rank == 0:
    print("#######################")
    print("# Logistic regression #")
    print("#######################")
    print("\n")
    print("We train for", epochs, "epochs with an effective batch size of", beff, ".")

path = "./logit_data_n100000_d2.h5"#"./logit_data_n100000_d2.h5" #"/pfs/work7/workspace/scratch/ku4408-VL_ScalableAI/data/logit_data_n*_d*.h5"
dset1 = "data"
dset2 = "labels"

with h5py.File(path, "r") as f:
    chunk = int(f[dset1].shape[0]/size)
    if rank==size-1:
        data = np.array(f[dset1][rank*chunk:])
        labels = np.array(f[dset2][rank*chunk:])
    else:
        data = np.array(f[dset1][rank*chunk:(rank+1)*chunk])
        labels = np.array(f[dset2][rank*chunk:(rank+1)*chunk])

print(
        "Rank", rank, "/", size, ": Local data has", data.shape[0], 
        "samples with", data.shape[1],"features and", labels.shape[0], 
        "labels. 0th elements are:", data[0], labels[0]
        )

# Bias trick: Prepend data with 1's for additional bias dimension.
ones = np.ones((data.shape[0], 1,))
data_bt = np.hstack([ones, data])

# Initialize model parameters randomly.
# After bias trick, weights have shape [n_features +1, ]
if rank == 0:
    weights = np.random.rand(data_bt.shape[1]) 
else:
    weights = np.zeros(data_bt.shape[1])

# Broadcast weights from root to other processors.
comm.Bcast(weights, root=0)

blocal = beff // size # Calculate local batch size.
print("Rank", rank, "/", size, ": Local batch size is", blocal, ".")
# Train model.
weights, loss_history, acc_history, Te = lr_train(weights, data_bt, labels, b=blocal, epochs=epochs)

if rank == 0:
    print("Final loss:", loss_history[-1], ", final accuracy:", acc_history[-1])
    print("Average training time per epoch:", Te, "s")
