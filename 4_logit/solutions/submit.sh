#!/bin/bash

#SBATCH --job-name=logit                   # job name
#SBATCH --partition=single                 # queue for the resource allocation.
#SBATCH --time=5:00                        # wall-clock time limit  
#SBATCH --mem=90000                        # memory per node
#SBATCH --cpus-per-task=40                 # number of CPUs required per MPI task
#SBATCH --ntasks-per-node=1                # maximum count of tasks per node
#SBATCH --mail-type=ALL                    # Notify user by email when certain event types occur.
#SBATCH --mail-user=marie.weiel@kit.edu    # notification email address


module purge                                    # Unload all currently loaded modules.
module load compiler/gnu/10.2                   # Load required modules.  
module load mpi/openmpi/4.0  
module load lib/hdf5/1.12.0-gnu-10.2-openmpi-4.0
source ~/.venvs/foobar/bin/activate  # Activate your virtual environment.

python logit.py 100 10
