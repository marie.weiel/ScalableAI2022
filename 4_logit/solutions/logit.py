import sys
import time
import h5py
import numpy as np
np.random.seed(842424) # Fix random seed for reproducibility.

def sigmoid(z):
    return 1.0 / (1.0+np.exp(-z))

def lr_predict(W, X):
    """
    Return prediction of logit model for training data X using model weights W.

    Parameters
    ----------
    X : array-like, shape = [n_samples, n_features +1]
        Training dataset (after bias trick).
    W : array-like, shape = [n_features + 1, ]
        Parameters (after bias trick).

    W: weights to be learned. There is one weight for every input dimension plus a bias.
    X: input data where the 0th input should be 1.0 to take the bias into account in a simple dot product.

    Returns:
    ----------
    array-like, shape = [n_samples, ]
    Predicted activations Y_est of logit model for training dataset X,
    in this case the sigmoid of dot product of weights and input data.
    """
    return sigmoid(X @ W)

def mse(Yest, Y):
    """"
    Compute mean-square-error loss.
    
    Parameters
    ----------
    Yest : array-like, shape = [n_samples, ]
           Predictions.
    Y : array-like, shape = [n_samples, ]
        Ground-truth labels.
        
    Returns
    ----------
    loss: MSE loss
    """
    N = Y.shape[0]                      # Get number of samples in considered batch. 
    loss = (1./N) * (Y-Yest).T @ (Y-Yest) # Calculate MSE loss for considered batch.
    return loss

def lr_loss(W, X, Y):
    """Return the loss and the gradient with respect to the weights.
    
    Parameters
    ----------
    W : weights of the model to be learned, where weights[0] is the bias
    X : input data of shape [N x D+1], 0th element of each sample is assumed to be 1
    Y : ground-truth labels of shape [N,]
    
    Returns
    -----------
    loss : scalar mean-square-error loss for batch of samples
    gradient : np.array([weights.shape]) 
               gradient of loss with respect to weights
    """
    N = Y.shape[0]          # Get number of samples in batch.
    Yest = lr_predict(W, X) # Compute logit prediction for all samples in batch. 
    
    loss = mse(Yest, Y)     # Compute MSE loss over all samples in batch.
    # Compute gradient vector of loss w.r.t. weights.
    gradient = (-2./N) * ((Y - Yest) * Yest * Yest * np.exp(-X @ W)).T @ X
    return loss, gradient

def lr_train(W, X, Y, epochs=100, eta=0.001, b=10):
    """
    Train the model, i.e. update the weights following the negative gradient until the model converges.
    
    Parameters
    ----------
    W : weights of the model to be learned, where weights[0] is the bias
    X : input data of shape [N x D+1], where each sample's 0th element is assumed to be 1 for bias trick
    Y : ground-truth labels of shape [N,]
    epochs : number of epochs to be trained
    eta : learning rate
    b : batch size
    
    Returns
    -----------
    weights : trained weights
    loss_history : history array with each epoch's loss
    acc_history : history array with each epoch's accuracy
    time : average training time per epoch
    """
    N = Y.shape[0] # Determine number of samples in batch.
    Nb = N // b    # Determine number of full batches in data (drop last).
    print("Data is divided into", Nb, "batches.")
    loss_history = np.zeros(epochs)
    acc_history = np.zeros(epochs)
    te = 0.0     # Initiate training time per epoch.
    
    for epoch in range(epochs): # Loop over epochs.
    # The number of epochs is a hyperparameter of gradient descent 
    # that controls the number of complete passes through the training dataset.
    # The batch size is a hyperparameter of gradient descent 
    # that controls the number of training samples to work through before the 
    # model’s internal parameters are updated.
        loss_sum = 0.0 # Initiate loss for each epoch.
        accuracy = 0.0 # Initiate accuracy for each epoch.
        
        start = time.perf_counter() # Start timer.
        
        for nb in range(Nb):
            x = X[nb*b:(nb+1)*b]
            y = Y[nb*b:(nb+1)*b]
            loss, gradient = lr_loss(W, x, y)
            loss_sum += loss
            
            corr = np.sum((lr_predict(W, x) + 0.5).astype(int) == y)
            accuracy += corr
            W -= eta * gradient
            
        end = time.perf_counter() # Stop timer.

        # Calculate loss + accuracy after each epoch.
        loss_sum /= Nb
        accuracy /= N
        accuracy *= 100
        
        # Append loss + accuracy of current epoch to history arrays.
        loss_history[epoch] = loss_sum
        acc_history[epoch] = accuracy
        te += (end - start)

        # Print every tenth epoch the training status.
        if epochs < 100: mod = 5
        elif epochs >= 100 and epochs < 1000: mod = 10
        elif epochs >= 1000 and epochs < 10000: mod = 100
        elif epochs >= 10000 and epochs < 100000: mod = 1000
        else: mod = 10000
        if epoch % mod == 0:
            print('Epoch: {:2}, Loss: {:.5f}, Accuracy: {}'.format(epoch, loss_sum, accuracy))
    te /= epochs
    return W, loss_history, acc_history, te

epochs = int(sys.argv[1])
b = int(sys.argv[2])

path = "./logit_data_n100000000_d2.h5"#"/pfs/work7/workspace/scratch/ku4408-VL_ScalableAI/data/logit_data_n10000_d2.h5"
dset1 = "data"
dset2 = "labels"

with h5py.File(path, "r") as f:
    data = np.array(f[dset1])
    labels = np.array(f[dset2])

print("We have", data.shape[0], "samples with", data.shape[1], "features and",labels.shape[0],"labels.")

# Bias trick: Prepend data with 1's for additional bias dimension.
ones = np.ones((data.shape[0], 1,))
data_bt = np.hstack([ones, data])
weights = np.random.rand(data_bt.shape[1]) # Initialize model parameters randomly.
weights, loss_history, acc_history, te = lr_train(weights, data_bt, labels, b=b, epochs=epochs)
print("Final loss:", loss_history[-1], ", final accuracy:", acc_history[-1])
print("Training time per epoch:", te, "s")
