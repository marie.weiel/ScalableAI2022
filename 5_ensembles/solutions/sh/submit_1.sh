#!/bin/bash

#SBATCH --job-name=RF1                     # job name
#SBATCH --partition=single                 # queue for the resource allocation
#SBATCH --time=24:00:00                    # wall-clock time limit  
#SBATCH --cpus-per-task=40
#SBATCH --mail-type=ALL                    # Notify user by email when certain event types occur.
#SBATCH --mail-user=u????@student.kit.edu  # notification email address

export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
module purge                          # Unload all currently loaded modules.
module load compiler/gnu/10.2   
module load mpi/openmpi/4.1   	      # Load all required modules.
module load devel/python/3.8.6_gnu_10.2
source ~/.venvs/sklearn/bin/activate  # Activate your virtual environment.

python random_forest_serial.py 9
