#!/bin/bash

#SBATCH --job-name=DL2                     # job name
#SBATCH --partition=multiple               # queue for the resource allocation
#SBATCH --nodes=2
#SBATCH --time=70:00                       # wall-clock time limit  
#SBATCH --mem=90000                        # memory per node
#SBATCH --ntasks-per-node=1                # maximum count of tasks per node
#SBATCH --cpus-per-task=40
#SBATCH --mail-type=ALL                    # Notify user by email when certain event types occur.
#SBATCH --mail-user=u????@student.kit.edu  # notification email address

export IBV_FORK_SAFE=1
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
module purge                          # Unload all currently loaded modules.
module load mpi/openmpi/4.1   	      # Load all required modules.
module load compiler/gnu/10.2   
module load devel/python/3.8.6_gnu_10.2
source ~/.venvs/sklearn/bin/activate  # Activate your virtual environment.

mpirun --mca mpi_warn_on_fork 0 python test_dataloaders.py
