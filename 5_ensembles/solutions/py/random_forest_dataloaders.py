# Sample data samples from CSV file with replacement
# using multiple processors in truly parallel fashion.
# Each processor needs to know
#   - overall number samples
#   - absolute byte positions of data samples' start / end as
#     array byte_pos = [[start0, end0], [start1, end1],...]
# Once I got this array:
# - Perform train-test split by randomly sampling `test_frac`
#   indices from overall number of samples xn = len(byte_pos).
#   Those samples are held out to form test dataset.
#   The other samples form global train dataset.
# - Assume p processors each of which holds sub random forest.
#   Each sub forest should train on `train_frac` * xn/p data samples,
#   where `train_frac` = 1 - `test_frac`.
#   For this, each processor should sample those `train_frac` * xn/p
#   data samples from global train dataset with replacement, i.e.,
#   globally, the random forest is trained on `train_frac` * xn samples,
#   however, some of them might occur multiple times while others might
#   not occur at all.
# To do so, each processor needs to have byte_pos array!

from mpi4py import MPI
from mpi4py.util.dtlib import from_numpy_dtype
import sys
import time
import os.path
import numpy as np
import random


def _determine_line_starts(f, comm, displs, counts):
    """
    Determine line starts in bytes from CSV file.

    Params
    ------
    f : file object
        handle of CSV file to read from
    comm : MPI communicator
           communicator to use
    displs : numpy array of length comm.size
             displacements of byte chunk starts on each rank
    counts : numpy array of length comm.size
             counts of byte chunk on each rank
    Returns
    -------
    line_starts : list
                  list of line starts in each byte chunk
    r : bytes object
        chunk read in on each rank
    lineter_len : int
                  number of chars used to indicate line end in file
    """
    # Set up communicator stuff.
    rank = comm.rank
    size = comm.size

    # Read bytes chunk and count linebreaks.
    # \r (Carriage Return) : Move cursor to line start w/o advancing to next line.
    # \n (Line Feed) : Move cursor down to next line w/o returning to line start.
    # \r\n (End Of Line) : Combination of \r and \n.

    lineter_len = 1  # Set number of line termination chars.
    line_starts = []  # Set up list to save line starts.
    f.seek(displs[rank])  # Jump to pre-assigned position in file.
    r = f.read(counts[rank])  # Read number of bytes from starting position.

    for pos, l in enumerate(r):  # Determine line breaks in bytes chunk.
        if chr(l) == "\n":  # Line terminated by '\n' only.
            if not chr(r[pos - 1]) == "\r": # No \r\n.
                line_starts.append(pos + 1)
        elif chr(l) == "\r":
            if (
                pos + 1 < len(r) and chr(r[pos + 1]) == "\n"
            ):  # Line terminated by '\r\n'.
                line_starts.append(pos + 2)
                lineter_len = 2
            else:  # Line terminated by '\r' only.
                line_starts.append(pos + 1)
    return line_starts, r, lineter_len


def _get_byte_pos_from_line_starts(
        line_starts, 
        file_size, 
        lineter_len
    ):
    """
    Get line starts and counts in byte from line starts to read lines via seek and read.

    Params
    ------
    line_starts : numpy array
                  absolute positions of line starts in byte
    file_size : int
                ansolute file size in byte
    lineter_len : int
                  number of line termination characters

    Returns
    -------
    lines_byte : numpy array
                 array containing vectors of (start, count) for lines in byte
    """
    lines_byte = [] # list for line starts and counts
    for idx in range(len(line_starts)): # Loop through all line starts.
        if idx == len(line_starts) - 1:  # Special case for last line.
            temp = [
                line_starts[idx],  # line start in bytes
                file_size
                - line_starts[idx]
                - lineter_len,  # bytes count of line length via difference
            ]
        else:  # all other lines
            temp = [
                line_starts[idx],  # line start in bytes
                line_starts[idx + 1] 
                - line_starts[idx] 
                - lineter_len,  # bytes count of line length via difference
            ]
        lines_byte.append(temp)
    return np.array(lines_byte)


def _decode_bytes_array(
        byte_pos, 
        f, 
        sep=",", 
        encoding="utf-8"
    ):
    """
    Decode lines from byte positions and counts.

    Params
    ------
    byte_pos : array
               vectors of line starts and lengths in bytes
    f : file object
        handle of CSV file to read from
    sep : char
          character used in file to separate entries
    encoding : str
               endoding used to decode entries from bytes

    Returns
    -------
    lines : list of data entries
            values read from CSV file as numpy array entries
            in float format
    """
    lines = [] # list for saving decoded lines
    byte_pos = byte_pos[byte_pos[:, 0].argsort()] # Sort line starts of data items to read from file in ascending order.
    for item in byte_pos:
        f.seek(item[0])  # Go to line start.
        line = f.read(item[1]).decode(
            encoding
        )  # Read specified number of bytes and decode.
        if len(line) > 0:
            sep_values = [
                float(val) for val in line.split(sep)
            ]  # Separate values in each line.
            line = np.array(
                sep_values
            )  # Convert list of separated values to numpy array.
        lines.append(line)  # Append numpy data entry to output list.
    return lines


def _split_indices_train_test(
        n_samples, 
        train_split, 
        seed
    ):
    """
    Make index-based train test split with shuffle.
    
    Params
    ------
    n_samples : int
                overall number of samples in dataset
    train_split : float
                  fraction of dataset used for training
                  0 < train_split < 1
    seed : int
           seed used for random number generator
    
    Returns
    -------
    n_train_samples : int
                      number of train samples
    n_test_samples : int
                     number of test samples
    train_indices : numpy array
                    indices of samples in dataset used for training
    test_indices : numpy array
                   indices of samples in dataset used for testing
    """
    n_train_samples = int( train_split * n_samples )  # Determine number of train samples from split.
    n_test_samples =  n_samples - n_train_samples  # Determine number of test samples.
    rng = np.random.default_rng(seed=seed)  # Set same seed over all ranks for consistent train-test split.
    all_indices = np.arange(0, n_samples)  # Construct array of all indices.
    rng.shuffle(all_indices)  # Shuffle them.
    train_indices = all_indices[:n_train_samples]  # First `n_train_samples` indices form train set.
    test_indices = all_indices[n_train_samples:]  # Remaining ones form test set.
    return n_train_samples, n_test_samples, train_indices, test_indices


def load_data_csv_parallel(
        path_to_data,
        header_lines,
        comm=MPI.COMM_WORLD,
        train_split=0.75,
        sep=",",
        encoding="utf-8",
        verbose=True,
    ):
    """
    Load data from CSV file in truly parallel fashion.

    Params
    ------
    path_to_data : str
                   path to .csv file
    header_lines : int
                   number of header lines
    comm : MPI communicator
           communicator to use
    train_split : float
                  train-test split fraction
    sep : char
          character used in file to separate entries
    encoding : str
               encoding used to decode lines from bytes
    verbose : bool
              verbosity level

    Returns
    -------
    train_samples_local : numpy array
                          rank-dependent train samples
    train_targets_local : numpy array
                          rank-dependent train targets
    test_samples : numpy array
                   global test samples
    test_targets : numpy array
                   global test targets
    """
    # Set up communicator stuff.
    rank = comm.rank
    size = comm.size

    file_size = os.stat(path_to_data).st_size  # Get file size in bytes.

    # Determine displs + counts of bytes chunk to read on each rank.
    base = file_size // size  # Determine base chunk size for each process.
    remainder = file_size % size  # Determine remainder bytes.
    counts = base * np.ones(  # Construct array with each rank's chunk counts.
        (size,), dtype=int
    )
    if remainder > 0:  # Equally distribute remainder over respective ranks to balance load.
        counts[:remainder] += 1
    displs = np.concatenate(  # Determine displs via cumulative sum from counts.
        (np.zeros((1,), dtype=int), np.cumsum(counts, dtype=int)[:-1])
    )

    if rank == 0:
        print(f"File size is {file_size} bytes.")
        
    if rank == 0 and verbose:
        print(f"Displs {displs}, counts {counts} for reading bytes chunks from file.")

    with open(path_to_data, "rb") as f:  # Open csv file to read from.
        
        # Determine line starts in bytes chunks on each rank.
        line_starts, r, lineter_len = _determine_line_starts(f, comm, displs, counts)

        # On rank 0, add very first line.
        if rank == 0:
            line_starts = [0] + line_starts

        if verbose:
            print(f"[{rank}/{size}]: {len(line_starts)} line starts in chunk.")

        # Find correct starting point, considering header lines.
        # Allgather numbers of line starts in each chunk in `total_lines` array.
        total_lines = np.empty(size, dtype=int)
        comm.Allgather(
            [np.array(len(line_starts), dtype=int), MPI.INT], [total_lines, MPI.INT]
        )
        cumsum = list(np.cumsum(total_lines))
        # Determine rank where actual data lines start,
        # i.e. remove ranks only containing header lines.
        start = next(i for i in range(size) if cumsum[i] > header_lines)
        if verbose:
            print(f"[{rank}/{size}]: total_lines is {total_lines}.")
            print(f"[{rank}/{size}]: cumsum is {cumsum}.")
            print(f"[{rank}/{size}]: start is {start}.")

        if rank < start:  # Ranks containing only header lines.
            line_starts = []
        if rank == start:  # Rank containing header + data lines.
            rem = header_lines - (0 if start == 0 else cumsum[start - 1])
            line_starts = line_starts[rem:]

        # Share line starts of data samples across all ranks via Allgatherv.
        line_starts += displs[rank]  # Shift line starts on each rank according to displs.
        if verbose:
            print(f"[{rank}/{size}]: {len(line_starts)} line starts of shape {line_starts.shape} and type {line_starts.dtype} in local chunk: {line_starts}")
        count_linestarts = np.array( # Determine local number of line starts.
            len(line_starts), dtype=int
        )  
        counts_linestarts = np.empty( # Initialize array to allgather local numbers of line starts.
            size, dtype=int
        )  
        comm.Allgather([count_linestarts, MPI.INT], [counts_linestarts, MPI.INT])
        n_linestarts = np.sum( counts_linestarts )  # Determine overall number of line starts.
        displs_linestarts = (
            np.concatenate(  # Determine displacements of line starts from counts.
                (
                    np.zeros((1,), dtype=int),
                    np.cumsum(counts_linestarts, dtype=int)[:-1],
                ),
                dtype=int,
            )
        )
        if verbose and rank == 0:
            print(f"Overall {n_linestarts} linestarts.")
            print(f"Number of linestarts in each chunk is {counts_linestarts}.")
            print(f"Displs of linestarts in each chunk is {displs_linestarts}.")

        all_line_starts = np.empty( # Initialize array to allgatherv line starts from all ranks.
            (n_linestarts,), dtype=line_starts.dtype
        )  
        if verbose and rank == 0:
            print(
                f"Recvbuf {all_line_starts}, {all_line_starts.shape}, {all_line_starts.dtype}."
            )
        comm.Allgatherv(
            line_starts,
            [
                all_line_starts,
                counts_linestarts,
                displs_linestarts,
                from_numpy_dtype(line_starts.dtype),
            ],
        )
        # Line starts were determined as those positions following a line end.
        # But: There is no line after last line end in file.
        # Thus, remove last entry from all_line_starts.
        all_line_starts = all_line_starts[:-1]
        if rank == 0:
            print(f"After Allgatherv: All line starts: {all_line_starts}")
        # Construct array with line starts and lengths in bytes.
        print(f"[{rank}/{size}]: Construct array with line starts and lengths in bytes.")
        lines_byte = _get_byte_pos_from_line_starts(
                all_line_starts, 
                file_size, 
                lineter_len
            )

        # Make global train-test split.
        print(f"[{rank}/{size}]: Make global train-test split.")
        n_train_samples, n_test_samples, train_indices, test_indices = _split_indices_train_test(
                n_samples = len(lines_byte), 
                train_split = train_split, 
                seed = size
            )

        n_train_samples_local = n_train_samples // size  # Determine local train dataset size.
        remainder_train = n_train_samples % size  # Balance load.
        if rank < remainder_train:
            n_train_samples_local += 1

        # Construct held-out test dataset (same on each rank).
        print(f"[{rank}/{size}]: Decode {n_test_samples} test samples from file.")
        test_lines = _decode_bytes_array(lines_byte[test_indices], 
                f, 
                sep=",", 
                encoding="utf-8"
            )
        test_samples = np.array(test_lines)[:, 1:]
        test_targets = np.array(test_lines)[:, 0]
        if verbose:
            print_str = f"[{rank}/{size}]: Test samples: {test_samples[0]}\n"
            print_str += f"Test targets: {test_targets[0]}\n"
            print(print_str)

        # Construct train dataset (different on each rank).
        rng = np.random.default_rng(seed=rank)
        print(f"[{rank}/{size}]: Draw local {n_train_samples_local} train indices.")
        train_indices_local = rng.choice(train_indices, size=n_train_samples_local)
        print(f"[{rank}/{size}]: Decode train lines from file.")
        train_lines_local = _decode_bytes_array(
            lines_byte[train_indices_local], f, sep=",", encoding="utf-8"
        )
        train_samples_local = np.array(train_lines_local)[:, 1:]
        train_targets_local = np.array(train_lines_local)[:, 0]

    # Now, each rank holds a local train set (samples + targets)
    # and the global held-out test set (samples + targets).
    return train_samples_local, train_targets_local, test_samples, test_targets


def load_data_csv_root(
        path_to_data, 
        header_lines, 
        comm,
        seed,
        train_split=0.75, 
        sep=",", 
        verbose=True
    ):
    """
    Load data from CSV file via root process.

    Params
    ------
    path_to_data : str
                   path to .csv file
    header_lines : int
                   number of header lines
    comm : MPI communicator
           communicator to use
    seed : int
           seed used for sklearn train-test split
    train_split : float
                  train-test split fraction
    sep : char
          character used in file to separate entries
    verbose : bool
              verbosity level

    Returns
    -------
    train_samples_local : numpy array
                          rank-dependent train samples
    train_targets_local : numpy array
                          rank-dependent train targets
    test_samples : numpy array
                   global test samples
    test_targets : numpy array
                   global test targets
    """
    # Set up communicator stuff.
    rank = comm.rank
    size = comm.size

    from sklearn.model_selection import train_test_split

    if rank == 0:
        # Load data into numpy array.
        data = np.loadtxt(
                path_to_data, 
                dtype=float, 
                delimiter=",", 
                skiprows=header_lines
            )
        # Divide data into samples and targets.
        samples = data[:, 1:]
        targets = data[:, 0]
        # Perform train-test split.
        samples_train, samples_test, targets_train, targets_test = train_test_split(
                samples, 
                targets, 
                test_size=1-train_split, 
                random_state=seed
            )
        
        n_train_samples = len(samples_train) # Determine number of samples in train set.
        n_test_samples = len(samples_test) # Determine number of samples in test set.
        n_features = samples_train.shape[1] # Determine number of features.
        
        n_train_samples_local = n_train_samples // size # Determine rank-local number of train samples.
        remainder_train = n_train_samples % size
        # Determine load-balanced counts and displacements.
        train_counts = n_train_samples_local * np.ones(size, dtype=int)
        for idx in range(remainder_train):
            train_counts[idx] += 1
        train_displs = np.concatenate(
            (np.zeros(1, dtype=int), np.cumsum(train_counts, dtype=int)[:-1]), dtype=int
        )
        print(f"There are {n_train_samples} train and {n_test_samples} test samples.\nLocal train samples: {train_counts}")
        # Sample `n_train_samples` indices from train set with replacement.
        rng = np.random.default_rng(seed=size)
        train_indices = rng.choice(range(n_train_samples), size=n_train_samples)
        print(f"train_indices have shape {train_indices.shape}.")
        # Construct train dataset from drawn indices (repetitions possible!).
        samples_train_shuffled = samples_train[train_indices]
        targets_train_shuffled = targets_train[train_indices]
        send_buf_train_samples = [
            samples_train_shuffled,
            train_counts * n_features,
            train_displs * n_features,
            from_numpy_dtype(samples_train_shuffled.dtype),
        ]
        send_buf_train_targets = [
            targets_train_shuffled,
            train_counts,
            train_displs,
            from_numpy_dtype(targets_train_shuffled.dtype),
        ]
        send_buf_test_samples = [samples_test, from_numpy_dtype(samples_test.dtype)]
        send_buf_test_targets = [
            targets_test,
            from_numpy_dtype(targets_train_shuffled.dtype),
        ]
    else:
        train_counts = None
        train_displs = None
        n_features = None
        n_test_samples = None
        send_buf_train_samples = None
        send_buf_train_targets = None
        send_buf_test_samples = None
        send_buf_test_targets = None

    n_features = comm.bcast(n_features, root=0)
    n_test_samples = comm.bcast(n_test_samples, root=0)
    train_counts = comm.bcast(train_counts, root=0)
    train_displs = comm.bcast(train_displs, root=0)
    samples_train_local = np.empty((train_counts[rank], n_features), dtype=float)
    targets_train_local = np.empty((train_counts[rank],), dtype=float)
    recv_buf_train_samples = [
        samples_train_local,
        from_numpy_dtype(samples_train_local.dtype),
    ]
    recv_buf_train_targets = [
        targets_train_local,
        from_numpy_dtype(targets_train_local.dtype),
    ]
    if rank != 0:
        samples_test = np.empty((n_test_samples, n_features), dtype=float)
        targets_test = np.empty((n_test_samples,), dtype=float)
    comm.Scatterv(send_buf_train_samples, recv_buf_train_samples, root=0)
    comm.Scatterv(send_buf_train_targets, recv_buf_train_targets, root=0)
    comm.Bcast(samples_test, root=0)
    comm.Bcast(targets_test, root=0)
    if verbose:
        print_str = f"[{rank}/{size}]: Train samples after Scatterv have shape {samples_train_local.shape}.\n"
        print_str += f"Train targets after Scatterv have shape {targets_train_local.shape}.\n"
        print_str += f"Test samples after Bcast have shape {samples_test.shape}.\n"
        print_str += f"Test targets after Bcast have shape {targets_test.shape}."
        print(print_str)
    return samples_train_local, targets_train_local, samples_test, targets_test
