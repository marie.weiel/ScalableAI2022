import sys
import time
import os.path
import numpy as np
import random


def load_data_csv(
    path_to_data, header_lines, train_split=0.75, sep=",", verbose=True
):
    """
    Load data from CSV file via root process.

    Params
    ------
    path_to_data : str
                   path to .csv file
    header_lines : int
                   number of header lines
    train_split : float
                  train-test split fraction
    sep : char
          character used in file to separate entries
    verbose : bool
              verbosity level

    Returns
    -------
    train_samples_local : numpy array
                          rank-dependent train samples
    train_targets_local : numpy array
                          rank-dependent train targets
    test_samples : numpy array
                   global test samples
    test_targets : numpy array
                   global test targets
    """

    from sklearn.model_selection import train_test_split

    # Load data into numpy array.
    data = np.loadtxt(
            path_to_data, 
            dtype=float, 
            delimiter=",", 
            skiprows=header_lines
        )
    # Divide data into samples and targets.
    samples = data[:, 1:]
    targets = data[:, 0]
    # Perform train-test split.
    samples_train, samples_test, targets_train, targets_test = train_test_split(
            samples, 
            targets, 
            test_size=0.25, 
            random_state=42
        )
    return samples_train, targets_train, samples_test, targets_test


header_lines = 1
sep = ","
#path_to_data = "./datasets/data_samples_100000000_features_200_informative_150_redundant_50_classes_8.csv"
#path_to_data = "/pfs/work7/workspace/scratch/ku4408-random_forest/datasets/data_samples_500000_features_200_informative_150_redundant_50_classes_8.csv"
#path_to_data = "/pfs/work7/workspace/scratch/ku4408-random_forest/datasets/data_samples_1000000_features_20_informative_15_redundant_5_classes_4.csv"
path_to_data = "./SUSY.csv"
verbose = True
train_split = 0.75
print("Loading data...")
start_load = time.perf_counter()
(
    train_samples,
    train_targets,
    test_samples,
    test_targets,
) = load_data_csv(
    path_to_data, header_lines, train_split, sep
)
elapsed_load = time.perf_counter() - start_load
print_str = "Done...\n"
print_str += f"Train samples and targets have shapes {train_samples.shape} and {train_targets.shape}.\n"
print_str += f"First ten elements are: {train_samples[:10]} and {train_targets[:10]}\n"
print_str += f"Test samples and targets have shapes {test_samples.shape} and {test_targets.shape}.\n"
print_str += f"First ten elements are: {test_samples[:10]} and {test_targets[:10]}\n"
print_str += f"Time for data loading is {elapsed_load} s."
print(print_str)

from sklearn.ensemble import RandomForestClassifier
print("Set up classifier.")
model = RandomForestClassifier(
    n_estimators=100, random_state=int(sys.argv[1])
)
start_train = time.perf_counter()
print("Train.")
_ = model.fit(train_samples, train_targets)
acc = model.score(test_samples, test_targets)
elapsed_train = time.perf_counter() - start_train
print(
    f"Time for training is {elapsed_train} s.\n Accuracy is {acc}."
)
