# IMPORTS
from mpi4py import MPI
import time
from random_forest_dataloaders import load_data_csv_parallel, load_data_csv_root

# SETTINGS
comm = MPI.COMM_WORLD
rank = comm.rank
size = comm.size
header_lines = 0
sep = ","
encoding = "utf-8"
path_to_data = "SUSY.csv"
verbose = True
train_split = 0.75
seed = size

# MAIN
print(f"[{rank}/{size}]: Loading data using truly parallel dataloader...")
start = time.perf_counter()
(
    train_samples_local_par,
    train_targets_local_par,
    test_samples_par,
    test_targets_par,
) = load_data_csv_parallel(
    path_to_data, header_lines, comm, train_split, sep, encoding, verbose
)
elapsed_par = time.perf_counter() - start

print(f"[{rank}/{size}]: Loading data using root-based dataloader...")
start = time.perf_counter()
(
    train_samples_local_root,
    train_targets_local_root,
    test_samples_root,
    test_targets_root,
) = load_data_csv_root(path_to_data, header_lines, comm, seed, train_split, sep, verbose)
elapsed_root = time.perf_counter() - start

print_str = f"[{rank}/{size}]: Done...\n"
print_str += f"Parallel: Local train samples and targets have shapes {train_samples_local_par.shape} and {train_targets_local_par.shape}.\n"
print_str += f"Parallel: Global test samples and targets have shapes {test_samples_par.shape} and {test_targets_par.shape}.\n"
print_str += f"Parallel: Elapsed time is {elapsed_par} s.\n"
print_str += f"Root: Local train samples and targets have shapes {train_samples_local_root.shape} and {train_targets_local_root.shape}.\n"
print_str += f"Root: Global test samples and targets have shapes {test_samples_root.shape} and {test_targets_root.shape}.\n"
print_str += f"Root: Elapsed time is {elapsed_root} s.\n"
print(print_str)
