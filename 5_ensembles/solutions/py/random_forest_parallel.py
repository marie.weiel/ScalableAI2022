from mpi4py import MPI
from mpi4py.util.dtlib import from_numpy_dtype
import sys
import pickle
import copy
import time
import os.path
import numpy as np
import random
from random_forest_dataloaders import load_data_csv_parallel, load_data_csv_root


def train_random_forest(n_trees, random_state, train_samples, train_targets):
    """
    Train random forest classifier.
    Params
    ------
    n_trees : int
              number of trees in forest
    random_state : int
                   seed
    train_samples : numpy array
                    samples of train dataset
    train_targets : numpy array
                    targets of train dataset
    Returns
    -------
    model : sklearn.ensemble.RandomForestClassifier
            trained model
    """
    from sklearn.ensemble import RandomForestClassifier
    model = RandomForestClassifier(
        n_estimators=n_trees, random_state=random_state
    )
    _ = model.fit(train_samples, train_targets)
    return model


def get_tree_predictions(model, samples):
    """
    Get predictions of all sub estimators in random forest.
    Params
    ------
    model : sklearn.ensemble.RandomForestClassifier
            model
    samples : numpy array
              samples whose class to predict
    Returns
    -------
    tree_predictions : numpy array
                       array with predictions of each sub estimator on samples
    """
    tree_predictions = []
    for tree in model.estimators_: # Loop over sub estimators.
        tree_predictions.append(tree.predict(samples)) # Append prediction of each sub estimator to list.
    return np.array(tree_predictions)

def calc_majority_vote(tree_wise_predictions):
    """
    Calculate majority vote from tree-wise predictions.
    Params
    ------
    tree_wise_predictions : numpy array
                            array with tree-wise predictions
    """
    sample_wise_predictions = tree_wise_predictions.transpose()
    majority_vote = []
    for sample_preds in sample_wise_predictions:
        unique, class_counts = np.unique(sample_preds, return_counts=True)
        majority_vote.append(sample_preds[np.argmax(class_counts)])
    return np.array(majority_vote)


def allgatherv_tree_predictions(tree_predictions, comm):
    """
    Allgatherv tree-wise predictions from local sub forests
    Params
    ------
    tree_predictions : numpy array
                       array with tree-wise predictions
    """
    print(f"[{comm.rank}/{comm.size}]: tree_predictions {tree_predictions.shape}, {tree_predictions.dtype}")
    n_trees = tree_predictions.shape[0]
    n_samples = tree_predictions.shape[1]
    tree_counts = np.array(comm.allgather(n_trees))
    tree_displs = np.concatenate(
        (np.zeros(1, dtype=int), np.cumsum(tree_counts, dtype=int)[:-1]), dtype=int
    )
    print(f"[{comm.rank}/{comm.size}]: tree counts {tree_counts} and displs {tree_displs}")
    all_predictions = np.empty(
        (np.sum(tree_counts), n_samples), dtype=tree_predictions.dtype
    )
    print(f"[{comm.rank}/{comm.size}]: all_predictions {all_predictions.shape}, {all_predictions.dtype}")
    send_buf_pred = [copy.deepcopy(tree_predictions), from_numpy_dtype(tree_predictions.dtype)]
    recv_buf_pred = [
        all_predictions,
        n_samples * tree_counts,
        n_samples * tree_displs,
        from_numpy_dtype(all_predictions.dtype),
    ]
    print(f"[{comm.rank}/{comm.size}]: Directly before Allgatherv...")
    comm.Allgatherv(send_buf_pred, recv_buf_pred)
    return all_predictions


# SETTINGS
comm = MPI.COMM_WORLD # communication
rank = comm.rank
size = comm.size

header_lines = 0      # input data file specifications
sep = ","
encoding = "utf-8"
path_to_data = "./SUSY.csv"

dl = int(sys.argv[1]) # dataloader specifications
train_split = 0.75
seed = size
verbose = False       

# Load data.
print(f"[{rank}/{size}]: Loading data...")
if dl == 0:
    if rank == 0:
        print("Using truly parallel dataloader...")
    start = time.perf_counter()
    (
        train_samples_local,
        train_targets_local,
        test_samples,
        test_targets,
    ) = load_data_csv_parallel(
        path_to_data, header_lines, comm, train_split, sep, encoding, verbose
    )
    elapsed = time.perf_counter() - start

elif dl == 1:
    if rank == 0:
        print("Using root-based dataloader with Scatterv...")
    start = time.perf_counter()
    (
        train_samples_local,
        train_targets_local,
        test_samples,
        test_targets,
    ) = load_data_csv_root(path_to_data, header_lines, comm, seed, train_split, sep, verbose)
    elapsed = time.perf_counter() - start

print_str = f"[{rank}/{size}]: Done...\n"
print_str += f"Local train samples and targets have shapes {train_samples_local.shape} and {train_targets_local.shape}.\n"
print_str += f"Global test samples and targets have shapes {test_samples.shape} and {test_targets.shape}.\n"
print(print_str)
print(f"[{rank}/{size}]: Labels are {train_targets_local}")
Elapsed = comm.allreduce(elapsed, op=MPI.SUM) / size

if rank == 0:
    print(f"Elapsed time is {Elapsed} s.")

n_trees = 100
n_trees_local = n_trees // size
n_trees_remainder = n_trees % size

# Balance load.
if rank < n_trees_remainder:
    n_trees_local += 1

# Set up and train local random forest.
print(f"[{rank}/{size}]: Set up and train local random forest.")
start_train = time.perf_counter()
clf = train_random_forest(
        n_trees=n_trees_local, 
        random_state=rank, 
        train_samples=train_samples_local, 
        train_targets=train_targets_local
        )

# Get class predictions of sub estimators in each forest.
print(f"[{rank}/{size}]: Get predictions of individual sub estimators.")
tree_predictions = get_tree_predictions(clf, test_samples)

# `tree_predictions` array contains one vector for each tree with class predictions for all test samples.
# Final prediction of parallel random forest is majority vote over all sub estimators.
print(f"[{rank}/{size}]: Allgather predictions.")
all_predictions = allgatherv_tree_predictions(tree_predictions, comm)
# Now, each rank holds predictions of all sub estimators over complete test set.
# `sample_wise_predictions` contains one vector for each test sample with class predictions from all trees.

# Calculate majority vote.
print(f"[{rank}/{size}]: Calculate majority vote.")
majority_vote = calc_majority_vote(all_predictions)
acc_global = (test_targets == majority_vote).mean()
acc_local = clf.score(test_samples, test_targets)
print(f"[{rank}/{size}]: Local accuracy is {acc_local}.")
acc_local_med = np.median(comm.allgather(acc_local))
acc_local_min = comm.allreduce(acc_local, op=MPI.MIN)
acc_local_max = comm.allreduce(acc_local, op=MPI.MAX)
elapsed_train = time.perf_counter() - start_train
Elapsed_train = comm.allreduce(elapsed_train, op=MPI.SUM) / size
if rank == 0:
    print(f"Median local accuracy is {acc_local_med}. Min {acc_local_min} and max {acc_local_max}.")
    print(f"Global accuracy is {acc_global}.")
    print(f"Average train time is {Elapsed_train} s.")
    results = np.array([size, dl, Elapsed, Elapsed_train, acc_local_min, acc_local_med, acc_local_max, acc_global])
    np.save(f"./res/results_N{size}_dl{truly_parallel}.npy", results)
