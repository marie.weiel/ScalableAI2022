# Vorlesung 2400004 – Skalierbare Methoden der Künstlichen Intelligenz WS 22/23
Vorlesung der KIT-Fakultät Informatik am Karlsruher Institut für Technologie  

Kontakt:
- Dr. Charlotte Debus (<charlotte.debus@kit.edu>)
- Dr. Markus Götz (<markus.goetz@kit.edu>)
- Dr. Marie Weiel (<marie.weiel@kit.edu>)
***
### Kursthemen
- Skalierbarkeit von ML-/KI-Algorithmen
- Parallelisierungsstrategien und Performanz
- Anwendungsbeispiele

### Kursbeschreibung
Die Methoden der künstlichen Intelligenz (KI) haben in der letzten Dekade zu erstaunlichen Durchbrüchen in Wissenschaft und Technik geführt. Dabei zeichnet sich zunehmend ein Trend zur Verarbeitung von immer größeren Datenmengen und dem Einsatz von parallelen und verteilten Rechenressourcen ab [1]. Ein prominentes Beispiel ist der Maschinenübersetzungsalgorithmus Generative Pre-trained Transformer 3 (GPT-3) [2], welcher mit 175 Milliarden trainierbaren Parametern auf 285.000 Prozessorkernen und 10.000 Grafikkarten die Grenzen herkömmlicher KI-Hardware sprengt. In der Vorlesung werden den Studierenden die Parallelisierung und Skalierbarkeit verschiedener KI-Algorithmen nähergebracht. Hierbei liegt der Fokus auf den Vorteilen und Ansätzen des parallelen Rechnens für KI-Methoden, verschiedenen verfügbaren Softwarepaketen zur Implementierung sowie den algorithmenspezifischen Herausforderungen. Diese werden anhand verschiedener Beispiele und Algorithmenklassen dargestellt, um die vielfältigen Anwendungsmöglichkeiten für skalierbare künstliche Intelligenz zu illustrieren:

- Skalierbares unüberwachtes Lernen
- Skalierbares überwachtes Lernen
- Skalierbare neuronale Netze
- Skalierbare Ensemblemethoden
- Skalierbare Suchverfahren

Darüber hinaus werden Datenformate und -management, gängige Maschinenmodelle sowie der Einsatz neuartiger Hardware, z.B. Quantencomputer oder neuromorphe Geräte, diskutiert.	
***
Over the last decade, artificial intelligence (AI) methods have significantly advanced the state-of-the-art in science and engineering. One of the most prominent trends is an ever increasing amount of analzyed (training) data, necessitating the usage of parallel and distributed computational resources. A well-known example for this is the machine translation algorithm Generative Pre-trained Transformer 3 (GPT-3) [1]. With a total of 175 billion parameters trained on 285.000 processor cores as well as 10.000 GPUs, this model exceeds the capabilities of traditional AI hardware. In this lecture, students will learn about parallelization and scaling approaches for different AI algorithms. An emphasis is put on the advantages of parallel computing for AI, available software packages for implementation, and, majorly, the algorithmic design challenges. In line with this, examples from the following algorithmic classes will illustrate the potential use for scalable AI:

- unsupervised learning
- supervised learning
- neural networks
- ensemble methods
- (hyperparameter) search methods

In conjuction with the course topics, the students will also learn about supporting data formats, machine models, and the use of novel hardware, such as quantum computers or neuromorphic devices.
***
[1] Ben-Nun, Tal, and Torsten Hoefler. "Demystifying parallel and distributed deep learning: An in-depth concurrency analysis." ACM Computing Surveys (CSUR) 52.4 (2019): 1-43.

[2] Brown, Tom B., et al. "Language models are few-shot learners." arXiv preprint arXiv:2005.14165 (2020).

#### Vorlesungen
- Einführung (27.10.22)
- Grundbegriffe und **Fundamentalkonzepte**
    - KI-Grundbegriffe (27.10.22)
    - Parallele Hardware, Skalierbarkeit und skalierbare Programmierung (03.11.22)
- **Unüberwachtes** Lernen bzw. Clusteringverfahren
    - k-Means, k-Medians, PSRS (Parallel Sorting) (10.11.22)
    - DBSCAN, paarweise Distanzen (17.11.22)
    - Spektrales Clustering, hierarchisches Clustering (24.11.22)
- **Überwachtes** Lernen
    - Lineare und logistische Regression (01.12.22)
    - (Kaskadierende) Support-Vektor-Maschinen (SVM) (08.12.22)
- **Ensemblemethoden**
    - Entscheidungsbäume, Random Forests, Boosting, Voting (15.12.22)
- **Weihnachtsvorlesung** (22.12.22)
- **Neuronale Netze**
    - Datenparallelität (19.01.23)
    - Modellparallelität (26.01.23)
    - Hyperparameteroptimierung und neurale Architektursuche (NAS) (02.02.23)
    - Bioinspirierte Verfahren (09.02.23)
- **Neuartige Hardwaresysteme**
    - Quantenannealer und neuromorphe Geräte (16.02.23)

#### Übungen
Die Programmierübungen werden in Form von Jupyter-Notebooks bereitgestellt und erfordern Zugriff auf ein entsprechendes HPC-System (bwUniCluster, Zugänge werden wir zu Beginn der Vorlesung gemeinsam organisieren). Sie orientieren sich thematisch an den Inhalten der Vorlesung. Zusätzlich gibt es zu jeder Übung einen ergänzenden Foliensatz.

- Übung 1: Skalierbare Programmierung (08.11.22)
- Übung 2: k-Means Clustering (22.11.22)
- Übung 3: Paralleles Sortieren und verteilte paarweise Distanzen (06.12.22)
- Übung 4: Logistische Regression - Parallelisierung auf Ebene der Daten (20.12.22)
- Übung 5: Ensemblemethoden (17.01.23)
- Übung 6: Datenparallele neuronale Netze - AlexNet und Pytorch's DistributedDataParallel (31.01.23)
- Übung 7: Bioinspirierte Optimierungsmethoden - Partikelschwarmoptimierung und evolutionäre Algorithmen (14.02.23)

### Sprache / Language
Deutsch / German

### Programmiersprache /Programming language
Hauptsächlich Python (Pytorch)

### Vorwissen / Prior knowledge
- Programmierkenntnisse, optimal Python
- Konzepte paralleler Programmierung
- Grundbegriffe künstlicher Intelligenz und maschinellen Lernens

Hier finden Sie sämtliche Übungsmaterialien zur Vorlesung [Skalierbare Methoden der Künstlichen Intelligenz](https://ilias.studium.kit.edu/ilias.php?ref_id=1905192&cmdClass=ilobjcoursegui&cmd=view&cmdNode=wz:lw&baseClass=ilrepositorygui). 
Die Foliensätze sämtlicher Vorlesungen und Übungen finden Sie zu gegebener Zeit im ILIAS-Portal.
