#!/bin/bash

#SBATCH --job-name=cpu
#SBATCH --partition=single
#SBATCH --time=30:00 # wall-clock time limit
#SBATCH --mem=40000
#SBATCH --nodes=1
#SBATCH --mail-type=ALL
##SBATCH --mail-user=u????@student.kit.edu

# Set up modules.
module purge			# Unload all currently loaded modules.
module load devel/cuda/10.2	# Load required modules.
module load compiler/gnu/11.2
module load mpi/openmpi/4.1
module load lib/hdf5/1.12.1-gnu-11.2-openmpi-4.1
source ~/.venvs/foobar/bin/activate # Activate your virtual environment.

python -u ./kmeans.py
