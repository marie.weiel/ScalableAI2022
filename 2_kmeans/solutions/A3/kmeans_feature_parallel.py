#!/usr/bin/env python

import h5py
import time
import torch
from mpi4py import MPI

class KMeans:

    def __init__(self, n_clusters=8, init="random", max_iter=300, tol=-1.0):
        self.init = init             # initialization mode (default: random)
        self.max_iter = max_iter     # maximum number of iterations
        self.n_clusters = n_clusters # number of clusters
        self.tol = tol               # tolerance for convergence criterion

        self._inertia = float("nan")
        self._cluster_centers = None

    def _initialize_centroids(self, x):
        # Determine number of local cluster centers per MPI rank.
        print("Rank", MPI.COMM_WORLD.rank, "/", MPI.COMM_WORLD.size,": Shape of data [#samples, #features] =", list(x.shape))
        # As all samples are partially present on each rank, only shuffle on root to determine centroids.
        if rank==0: indices = torch.randperm(x.shape[0])[: self.n_clusters]
        # torch.randperm(n) returns random permutation of integers from 0 to n - 1.
        # x = data, x.shape[0] gives number of samples in data.
        # Choose first n_cluster randomly shuffled indices as centroid indices.
        else: indices = torch.empty(self.n_clusters, dtype=torch.int64)
        MPI.COMM_WORLD.Bcast(indices, root=0)
        cluster_centers = x[indices]
        self._cluster_centers = cluster_centers # Set centroids as class attribute.

    def _fit_to_cluster(self, x):
        squared_distances = torch.cdist(x, self._cluster_centers)**2# Calculate squared distance btwn samples and centroids for part of features.
        Squared_distances = torch.empty(squared_distances.shape, dtype=torch.float)
        MPI.COMM_WORLD.Allreduce(squared_distances, Squared_distances, op=MPI.SUM)   # Sum up squared distances over features.
        distances = torch.sqrt(Squared_distances)
        matching_centroids = distances.argmin(axis=1, keepdim=True) # Determine index of nearest centroid for each sample.
        return matching_centroids

    def fit(self, x):
        self._initialize_centroids(x)
        new_cluster_centers = self._cluster_centers.clone()

        # Iteratively fit points to centroids.
        for idx in range(self.max_iter):
            # Determine centroids.
            print("Rank", MPI.COMM_WORLD.rank, "/", MPI.COMM_WORLD.size, ": Iteration", idx, "...")
            matching_centroids = self._fit_to_cluster(x) # Redundantly determine index of nearest centroid for each sample.

            # Update centroids.
            for i in range(self.n_clusters):
                # Locally determine samples in currently considered cluster i (binary encoding).
                selection = (matching_centroids == i).type(torch.int64)

                # Locally accumulate samples and total number of samples in cluster i.
                assigned_points = (x * selection).sum(axis=0, keepdim=True)    # Sum up samples in cluster i.
                points_in_cluster = selection.sum(axis=0, keepdim=True).clamp( # Determine overall number of samples in cluster i.
                    1.0, torch.iinfo(torch.int64).max
                )

                # Compute new centroids.
                new_cluster_centers[i : i + 1, :] = assigned_points / points_in_cluster.clamp(
                    1.0, torch.iinfo(torch.int64).max
                )

            # Check whether centroid movement has converged.
            inertia = ((self._cluster_centers - new_cluster_centers) ** 2).sum()
            #if rank==0: print("Current inertia =", inertia)
            Inertia = torch.empty(inertia.shape)
            MPI.COMM_WORLD.Allreduce(inertia, Inertia, op=MPI.SUM)
            self._inertia = Inertia
            self._cluster_centers = new_cluster_centers.clone()
            if self.tol is not None and self._inertia <= self.tol:
                break
        return self

rank = MPI.COMM_WORLD.rank
size = MPI.COMM_WORLD.size

if rank==0: 
    print("###############################################")
    print("# Feature-parallel PyTorch k-means clustering #")
    print("###############################################")

path = "/pfs/work7/workspace/scratch/ku4408-VL_ScalableAI/data/cityscapes_300.h5"
dataset = "cityscapes_data"

if rank==0:
    print("\nLoading data... {}[{}]".format(path, dataset), end="")
    print("\n")

with h5py.File(path, "r") as handle:
    chunk = int(handle[dataset].shape[1]/size)
    if rank==size-1: data = torch.tensor(handle[dataset][:,rank*chunk:])
    else: data = torch.tensor(handle[dataset][:,rank*chunk:(rank+1)*chunk])

print("Rank", MPI.COMM_WORLD.rank,"/", MPI.COMM_WORLD.size,": \t[OK]")

# k-means parameters
num_clusters = 8
num_iterations = 20

kmeans = KMeans(n_clusters=num_clusters, max_iter=num_iterations)
if rank==0:
    print("Start fitting the data...")
    start = time.perf_counter() # Start runtime measurement.

kmeans.fit(data)            # Perform actual k-means clustering.

if rank==0:
    end = time.perf_counter()   # Stop runtime measurement.
    print("DONE.")
    print("Run time:","\t{}s".format(end - start), "s")
