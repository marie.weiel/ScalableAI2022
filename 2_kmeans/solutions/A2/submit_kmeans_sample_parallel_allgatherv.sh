#!/bin/bash

#SBATCH --job-name=kmeans_sample        # job name
#SBATCH --partition=multiple        # queue for resource allocation
#SBATCH --nodes=4                       # number of nodes to be used
#SBATCH --time=30:00                    # wall-clock time limit
#SBATCH --mem=40000                     # memory per node 
#SBATCH --cpus-per-task=40              # number of CPUs required per MPI task
#SBATCH --ntasks-per-node=1             # maximum count of tasks per node
#SBATCH --mail-type=ALL                 # Notify user by email when certain event types occur.
##SBATCH --mail-user=u????@student.kit.edu

# Set up modules.
module purge				# Unload all currently loaded modules.
module load devel/cuda/10.2		# Load required modules.
module load compiler/gnu/11.2
module load mpi/openmpi/4.1
module load lib/hdf5/1.12.1-gnu-11.2-openmpi-4.1
source ~/.venvs/foobar/bin/activate # Activate your virtual environment.

mpirun python -u ./kmeans_sample_parallel_allgatherv.py
