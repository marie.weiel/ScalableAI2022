#!/usr/bin/env python

import h5py
import time
import torch
from mpi4py import MPI

class KMeans:

    def __init__(self, n_clusters=8, init="random", max_iter=300, tol=-1.0):
        self.init = init             # initialization mode (default: random)
        self.max_iter = max_iter     # maximum number of iterations
        self.n_clusters = n_clusters # number of clusters
        self.tol = tol               # tolerance for convergence criterion

        self._inertia = float("nan")
        self._cluster_centers = None

    def _initialize_centroids(self, x):
        # Determine number of local cluster centers per MPI rank.
        n_clusters_local = self.n_clusters // MPI.COMM_WORLD.size # Assume n_clusters >= MPI.COMM_WORLD.size
        indices = torch.randperm(x.shape[0])[: n_clusters_local]
        # torch.randperm(n) returns random permutation of integers from 0 to n - 1.
        # x = data, x.shape[0] gives number of samples in data.
        # Choose first n_cluster_local randomly shuffled indices as centroid indices.
        x_local = x[indices]
        print("Rank", MPI.COMM_WORLD.rank,"/", MPI.COMM_WORLD.size, ": Local cluster centers have shape [#samples, #features] =", list(x_local.shape),".")
        # Initialize empty tensor on each rank to store all centroids.
        # Required shape is [# clusters, # features].
        ############ REQUIRED FOR SAMPLE-PARALLEL
        cluster_centers = torch.empty((self.n_clusters, x.shape[1]), dtype=torch.float)
        MPI.COMM_WORLD.Allgather([x_local, MPI.FLOAT], [cluster_centers, MPI.FLOAT])
        ############
        if rank==0: print("Global cluster centers have shape [#samples, #features] =", list(cluster_centers.shape),".")
        self._cluster_centers = cluster_centers # Set centroids as class attribute.

    def _fit_to_cluster(self, x): # Return
        distances = torch.cdist(x, self._cluster_centers)           # Calculate euclidian distances btwn samples and centroids.
        matching_centroids = distances.argmin(axis=1, keepdim=True) # Determine index of nearest centroid for each sample.
        return matching_centroids

    def fit(self, x):
        self._initialize_centroids(x) # Initialize centroids.
        new_cluster_centers = self._cluster_centers.clone()

        # Iteratively fit points to centroids.
        for idx in range(self.max_iter):
            # Determine nearest centroids.
            print("Rank", MPI.COMM_WORLD.rank, "/", MPI.COMM_WORLD.size, ": Iteration", idx, "...")
            matching_centroids = self._fit_to_cluster(x) # Determine index of nearest centroid for each sample.

            # Update centroids.
            for i in range(self.n_clusters):
                # Locally determine samples in currently considered cluster i (binary encoding).
                selection = (matching_centroids == i).type(torch.int64)

                # Locally accumulate samples and total number of samples in cluster i.
                # Local directed sum of samples in cluster i.
                assigned_points = (x * selection).sum(axis=0, keepdim=True)
                ######### REQUIRED FOR SAMPLE-PARALLEL: clamp to 0 before communication
                # Local number of samples in cluster i.
                points_in_cluster = selection.sum(axis=0, keepdim=True).clamp(
                    0.0, torch.iinfo(torch.int64).max
                )

                # Communicate.
                Assigned_points = torch.empty(assigned_points.shape, dtype=torch.float)
                Points_in_cluster = torch.empty(points_in_cluster.shape,dtype=torch.int64)
                MPI.COMM_WORLD.Allreduce(assigned_points, Assigned_points, op=MPI.SUM)
                MPI.COMM_WORLD.Allreduce(points_in_cluster, Points_in_cluster, op=MPI.SUM)
                #########
                # Compute new centroids.
                new_cluster_centers[i : i + 1, :] = Assigned_points / Points_in_cluster.clamp(
                    1.0, torch.iinfo(torch.int64).max
                )

            # Check whether centroid movement has converged.
            self._inertia = ((self._cluster_centers - new_cluster_centers) ** 2).sum()
            self._cluster_centers = new_cluster_centers.clone()
            if self.tol is not None and self._inertia <= self.tol:
                break
        return self

rank = MPI.COMM_WORLD.rank
size = MPI.COMM_WORLD.size

if rank==0: 
    print("##############################################")
    print("# PyTorch sample-parallel k-means clustering #")
    print("##############################################")

path = "/pfs/work7/workspace/scratch/ku4408-VL_ScalableAI/data/cityscapes_300.h5"
dataset = "cityscapes_data"

if rank==0:
    print("\nLoading data... {}[{}]".format(path, dataset), end="")
    print("\n")

with h5py.File(path, "r") as handle:
    chunk = int(handle[dataset].shape[0]/size)
    if rank==size-1: data = torch.tensor(handle[dataset][rank*chunk:],dtype=torch.float)
    else: data = torch.tensor(handle[dataset][rank*chunk:(rank+1)*chunk],dtype=torch.float)

print("Rank", MPI.COMM_WORLD.rank,"/", MPI.COMM_WORLD.size,": \t[OK]")

# k-means parameters
num_clusters = 8
num_iterations = 20

kmeans = KMeans(n_clusters=num_clusters, max_iter=num_iterations)
if rank==0:
    print("Start fitting the data...")
    start = time.perf_counter() # Start runtime measurement.

kmeans.fit(data)            # Perform actual k-means clustering.

if rank==0:
    end = time.perf_counter()   # Stop runtime measurement.
    print("DONE.")
    print("Run time:","\t{}s".format(end - start), "s")
